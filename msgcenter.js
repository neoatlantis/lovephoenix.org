/* Task:
 * 1. Works as an EventEmitter, collects a new message, and tells other
 *    connections to send a new message, if proper.
 * 2. Record all messages, and provide query service for single connections.
 */

var util = require('util'),
    events = require('events');

function MessageCenter(){
    var self = this;
    events.EventEmitter.call(this);

    this.broadcast = function(msg, sender){
        /* `sender` is assumed to be clean since it should be provided from
         * `session/message.js`. */
        if(!$crypto$.util.type(msg).isString()) return;
        self.emit('chat', {
            message: msg,
            sender: sender,
        });
        // record message to log
        //TODO
    };

    return this;
};
util.inherits(MessageCenter, events.EventEmitter);


module.exports = function(){
    return new MessageCenter();
}
