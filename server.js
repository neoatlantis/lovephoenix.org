#!/usr/bin/node

// -------- some global scope things

$crypto$ = require('./crypto.js');
Promise = require('promise');



var express = require('express'),
    website = express();
var server = require('http').Server(website);
var io = require('socket.io')(server);

var storageEngine = require('./kvstorage.js'),
    messageCenter = require('./msgcenter.js')();

// -------- start http and socket.io, serve static files

website.use('/', express.static('public'));
server.listen(4000);

// -------- start account manager, presence manager

var account = require('./account.js')(storageEngine);
var presence = require('./presence.js')(storageEngine);



require('./io.connection.js')({
    io: io,
    storageEngine: storageEngine,
    account: account,
    presence: presence,
    msgcenter: messageCenter,
});
