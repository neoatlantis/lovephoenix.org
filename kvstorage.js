var storage = require('node-persist');
storage.initSync();

// ---- set up automatic persist

function saveStorageAsync(){
    storage.persist();
}
setInterval(saveStorageAsync, 30000);

var allowExit = false;
function saveStorageOnExit(){
    if(allowExit) return;
    console.log("Persist storage synchronously.");
    try{
        storage.persistSync();
        allowExit = true;
        process.exit();
    } catch(e){
    }
}

/*process.on('exit', saveStorageOnExit);
process.on('SIGINT', saveStorageOnExit);
process.on('uncaughtException', saveStorageOnExit);*/

// ---- define storage

function PrefixedAccess(prefix){
    var self = this;

    function getKey(key){ return prefix + '-' + key; }

    this.setItem = function(key, value, callback){
        return storage.setItem(getKey(key), value, callback);
    };

    this.setItemSync = function(key, value){
        return storage.setItemSync(getKey(key), value);
    };

    this.getItem = function(key, callback){
        return storage.getItem(getKey(key), callback);
    };

    this.getItemSync = function(key){
        return storage.getItemSync(getKey(key));
    };

    this.removeItem = function(key, callback){
        return storage.removeItem(getKey(key), callback);
    };

    this.removeItemSync = function(key){
        return storage.remoteItemSync(getKey(key));
    };
    

    return this;
}



function Storage(){
    var self = this;

    this.general = new PrefixedAccess('general');
    this.privateKey = new PrefixedAccess('privatekey');
    this.buddyList = new PrefixedAccess('buddylist');


    return this;
}


module.exports = function getStorage(){ return new Storage(); };
