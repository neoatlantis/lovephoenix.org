/* Presence and key mangement
 *
 * This provides a global private/public key management system, and provides
 * the client with a list function for refreshing all users' presence status
 * with their public keys.
 */
var util = require('util'),
    events = require('events');

function PresenceManager(storageEngine){
    var self = this;
    events.EventEmitter.call(this);
    var storage = storageEngine();

    var presenceStatus = {};

    /* setOnline() and setOffline() are designed for `session/auth.js` for
     * registering online/offline events. It's `username` argument is therefore
     * assumed to be clean. */

    this.setOnline = function(username){
        if(!presenceStatus[username]) presenceStatus[username] = {};
        presenceStatus[username].online = true;
        self.emit('presence online', username);
    };

    this.setOffline = function(username){
        if(!presenceStatus[username]) presenceStatus[username] = {};
        presenceStatus[username].online = false;
        self.emit('presence offline', username);
    };




    return this;
};
util.inherits(PresenceManager, events.EventEmitter);

module.exports = function(storageEngine){
    return new PresenceManager(storageEngine);
}
