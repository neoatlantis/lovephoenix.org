var sessionFuncs = [
    'auth',
    'message',
    'account-reg',
    'presence',
    'privatekey',
];


module.exports = function(resource){
    resource.io.on('connection', function(socket){
        // On each new connection

        var $session$ = {
            io: resource.io,
            socket: socket,
            session: {},
            storage: resource.storageEngine(),
            account: resource.account,
            presence: resource.presence,
            msgcenter: resource.msgcenter,
        };

        for(var i=0; i<sessionFuncs.length; i++){
            require('./session/' + sessionFuncs[i] + '.js')($session$);
        }

        $session$.socket.emit('connection confirmed');
        console.log("Accepted new connection.");
    });
};
