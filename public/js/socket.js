(function(){

    var socket = false;
    define(['socketio'], function(io){
        if(!socket) socket = io();
        return socket; 
    });

})();
