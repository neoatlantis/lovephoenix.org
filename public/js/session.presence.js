/* Presence and Crypto UI management
 *
 * This module communcates presence data of local user and buddies with the
 * server. The public keys are also part of a presence information. It also
 * manages the UI for crypto purposes.
 */

require(['session', 'crypto.interface'], function($session$, ci){
//////////////////////////////////////////////////////////////////////////////

var $crypto$ = ci();
    
function onPresenceOnline(username){
    var msg = "[" + username + "] has entered chatroom.";
    $('#messages').append($('<li>').addClass('system').text(msg));
    console.log(msg);
}
$session$.socket.on('presence online', onPresenceOnline);

function onPresenceOffline(username){
    var msg = "[" + username + "] has left chatroom.";
    $('#messages').append($('<li>').addClass('system').text(msg));
    console.log(msg);
}
$session$.socket.on('presence offline', onPresenceOffline);


// -------- deal with crypto ui

$('#dialog-new-privatekey').dialog({
    autoOpen: false,
    modal: true,
    resizable: false,
    minHeight: 180,
    minWidth: 300,
});

$('#dialog-decrypt-privatekey').dialog({
    dialogClass: 'no-close',
    autoOpen: false,
    closeOnEscape: false,
    modal: true,
    resizable: false,
    minHeight: 180,
    minWidth: 300,
});

//////////////////////////////////////////////////////////////////////////////
});
