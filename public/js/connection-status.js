require(['socket'], function(socket){
//////////////////////////////////////////////////////////////////////////////

function onConnected(){
    $('#page-not-connected').hide();
    $('#page-connected').show();
    console.log('Connected to server.');
}
socket.on('connect', onConnected);

function onDisconnected(){
    $('#page-not-connected').show();
    $('#page-connected').hide();
    console.log("Disconnected from server");
}
socket.on('disconnect', onDisconnected);

//////////////////////////////////////////////////////////////////////////////
});
