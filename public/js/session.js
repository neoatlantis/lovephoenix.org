define(['socket'], function(socket){
    $session$ = {
        socket: socket,
        username: false,
        password: false,
        verified: false,
        loggedIn: false,
    };
    return $session$;
});
