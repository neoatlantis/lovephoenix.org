require(['session'], function($session$){
//////////////////////////////////////////////////////////////////////////////

// -------- chat page interactions

function onChatFailed(reason){
    console.log('Failed sending message.', reason);
    if('need-login' == reason){
        $session$._doLogout();
        return;
    }
}
$session$.socket.on('chat failed', onChatFailed);

$('form').submit(function(){
    var msg = $('#m').val().trim();
    if(!msg) return false;
    $session$.socket.emit('chat', msg);
    $('#m').val('');
    return false;
});

function onChat(msg){
    if(!msg) return;
    if(msg.sender == $session$.username)
        $('#messages').append($('<li>').addClass('local').text(msg.message));
    else
        $('#messages').append($('<li>').addClass('buddy').text(msg.message));
}
$session$.socket.on('chat', onChat);


//////////////////////////////////////////////////////////////////////////////
});
