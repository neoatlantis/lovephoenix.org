require(['session'], function($session$){
//////////////////////////////////////////////////////////////////////////////

$session$._doLogout = doLogout;

// --------- session management: authentication status determines whether
//           login or chat

function initSession(){
    if(!$session$.loggedIn){
        // not logged in
        displayAuthenticationPage();
    } else {
        // logged in, just reconnect
        doLogin();
    }
}
$session$.socket.on('connect', initSession);

function doLogin(username, password){
    if(username && password){
        $session$.username = username;
        $session$.password = password;
        $session$.verified = false;
    } else {
        if(!$session$.verified) return;
    }
    $session$.socket.emit('session login', {
        username: $session$.username,
        password: $session$.password,
    });
};

function doLogout(){
    $session$.socket.emit('session logout');
};

function onSessionLoginSuccessful(){
    console.log("Login successful.");
    $session$.verified = true;
    $session$.loggedIn = true;
    displayChatPage();
};
$session$.socket.on('session login successful', onSessionLoginSuccessful);

function onSessionLogoutSuccessful(){
    /* This is a signal that can be sent by the server proactively to inform
     * that a session has been terminated, e.g. by another successful
     * authentication. So do not mix this just with the `doLogout` function.
     */
    console.log("Logout successful.");
    $session$.loggedIn = false;
    displayAuthenticationPage();
}
$session$.socket.on('session logout successful', onSessionLogoutSuccessful);

function displayAuthenticationPage(){
    $('#page-auth').show();
    $('#page-chat').hide();
};

function displayChatPage(){
    $('#page-auth').hide();
    $('#page-chat').show();
};

// -------- login/register: related functions on #page-auth for login or
//          register new account

function onLoginFormSubmission(){
    var username = $('#login input[name="username"]').val(),
        password = $('#login input[name="password"]').val();
    doLogin(username, password);
};
$('#login').submit(onLoginFormSubmission);

function onRegisterFormSubmission(){
};

//////////////////////////////////////////////////////////////////////////////
});
