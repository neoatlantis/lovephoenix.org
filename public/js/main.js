requirejs.config({
    baseUrl: 'js',
    paths: {
        'socketio': '/socket.io/socket.io',
        'jquery': '/js/ext/jquery',
        'jqueryui': '/js/ext/jqueryui',
        'crypto': '/js/ext/crypto',
        'lodash': "/js/ext/lodash",
        'postal': "/js/ext/postal",
    }
});

require(['jquery'], function($){ $(function(){

    require(['jqueryui'], function(){ 

        require([
            'connection-status',

            'session.auth',
            'session.chat',
            'session.presence',
        ], function(){
            
        });

    });

}); });
