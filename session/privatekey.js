/* Private key storage IO on the server side.
 *
 * The server acts as simply a storage. The client itself is responsible for
 * properly encrypting the private key before sending it to the server. Also
 * the public key will not be saved, rather it's up to the client to derivate
 * the public key on-the-fly after retrieving, and publishing it through the
 * presence system.*/

module.exports = function($session$){
//////////////////////////////////////////////////////////////////////////////

$session$.socket.on('privatekey get', function(){
    var msg = {
        username: $session$.session.username,
        uuid: $session$.session.uuid,
    };
    var p = $session$.account.getPrivateKey(msg); // validation done inside
    p.done(function(privatekey){
        $session$.socket.emit('privatekey get successful', privatekey);
    }, function(reason){
        $session$.socket.emit('privatekey get failed', reason);
    }); 
});

$session$.socket.on('privatekey set', function(newkey){
    var msg = {
        username: $session$.session.username,
        uuid: $session$.session.uuid,
        privateKey: newkey,
    };
    var p = $session$.account.setPrivateKey(msg); // validation done inside
    p.done(function(){
        $session$.socket.emit('privatekey set successful', newkey);
    }, function(reason){
        $session$.socket.emit('privatekey set failed', reason);
    });
});


//////////////////////////////////////////////////////////////////////////////
}
