module.exports = function($session$){
//////////////////////////////////////////////////////////////////////////////

console.log('Account reg loaded.');

$session$.socket.on('account register', function(msg){
    console.log("Register new account.");
    var p = $session$.account.register(msg);
    p.then(function(){
        console.log("Register new account successful.");
        $session$.socket.emit('account register successful');
    }, function(reason){
        console.log("Register new account failed.");
        $session$.socket.emit('account register failed', reason);
    });
});


//////////////////////////////////////////////////////////////////////////////
}
