/* Delivers presence list and updates. */
module.exports = function($session$){
//////////////////////////////////////////////////////////////////////////////

var onPresenceOnline = function(username){
    if(!$session$.session.uuid) return;
    $session$.socket.emit('presence online', username);
};

var onPresenceOffline = function(username){
    if(!$session$.session.uuid) return;
    $session$.socket.emit('presence offline', username);
};

function removePresenceSubscription(){
    $session$.presence.removeListener('presence online', onPresenceOnline);
    $session$.presence.removeListener('presence offline', onPresenceOffline);
};

function addPresenceSubscription(){
    $session$.presence.on('presence online', onPresenceOnline);
    $session$.presence.on('presence offline', onPresenceOffline);
};

$session$.socket.on('disconnect', removePresenceSubscription);
addPresenceSubscription();

//////////////////////////////////////////////////////////////////////////////
}
