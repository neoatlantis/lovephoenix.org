module.exports = function($session$){
//////////////////////////////////////////////////////////////////////////////

var monitorSessionValid = function(){
    /* A session validity monitor. If backend `account.js` closed a session,
     * this session will also be invalidated. */
    if(!$session$.session.uuid) return;
    var valid = $session$.account.sessionValid(
        $session$.session.username,
        $session$.session.uuid
    );
    if(!valid){
        onSessionLogout();
        return;
    };
    setTimeout(monitorSessionValid, 1000);
}

// -------- dealing with session login/logout

$session$.socket.on('session login', function(msg){
    var p = $session$.account.login(msg);
    p.done(function(uuid){
        $session$.session.username = msg.username;
        $session$.session.uuid = uuid;
        monitorSessionValid();
        $session$.socket.emit('session login successful');
        $session$.presence.setOnline(msg.username);
        console.log("User [" + msg.username + "] logged in.");
    }, function(r){
        $session$.socket.emit('session login failed', r);
    });
});

function onSessionLogout(){
    // if not logged in, ignore this.
    if(!$session$.session.username) return;
    // always pretend to have been logged out successful.
    $session$.socket.emit('session logout successful');
    // we do not need the client to supply a username, which can be fraud.
    var msg = {
        username: $session$.session.username,
        uuid: $session$.session.uuid,
    };
    $session$.account.logout(msg);
    $session$.session.uuid = false;
    $session$.presence.setOffline(msg.username);
    console.log("User [" + msg.username + "] logged out.");
};

$session$.socket.on('session logout', onSessionLogout);
$session$.socket.on('disconnect', onSessionLogout);


//////////////////////////////////////////////////////////////////////////////
}
