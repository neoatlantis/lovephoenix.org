module.exports = function($session$){
//////////////////////////////////////////////////////////////////////////////

function onBroadcastedChat(msg){
    if(!$session$.session.uuid) return;
    $session$.socket.emit('chat', msg);
};

$session$.msgcenter.on('chat', onBroadcastedChat);
$session$.socket.on('disconnect', function(){
    $session$.msgcenter.removeListener('chat', onBroadcastedChat);
});


$session$.socket.on('chat', function(msg){
    // must be logged in
    if(!$session$.session.uuid){
        return $session$.socket.emit('chat failed', 'need-login');
    };
    // broadcast message using MessageCenter
    $session$.msgcenter.broadcast(msg, $session$.session.username);
});


//////////////////////////////////////////////////////////////////////////////
}
