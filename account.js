var scrypt = require('scrypt');
var buffer = require('buffer');

function promiseScryptHash(password){
    return new Promise(function(fulfill, reject){
        var p = scrypt.kdf(password, {"N":1,"r":1,"p":1});
        p.then(function(s){
            fulfill(s.toString('base64'));
        }, function(){
            reject('scrypt-hash-error');
        });
    });
};

function promiseScryptVerifyHash(password, passwordHash){
    try{
        password = new buffer.Buffer(password);
        passwordHash = new buffer.Buffer(passwordHash, 'base64');
    } catch(e){
        return Promise.reject('scrypt-verify-error');
    }
    return new Promise(function(fulfill, reject){
        var p = scrypt.verifyKdf(passwordHash, password);
        p.done(function(result){
            fulfill(Boolean(result));
        }, function(){
            fulfill(false);
        });
    });
};



function AccountManager(storage){
    var self = this;

    function isUsername(s){
        return /^[0-9a-zA-Z_]{4,20}$/.test(s);
    }

    function isPrivateKey(s){
        return /^[0-9a-zA-Z=\+\/]{1,512}$/.test(s);
    }

    function isPassword(s){
        if(!$crypto$.util.type(s).isString()) return false;
        return s.length > 8;
    }

    function getEntryKeyFromUsername(s){ return s.toLowerCase(); }

    this.register = function(msg){
        var username = msg.username, password = msg.password;
        if(!isUsername(username)) 
            return Promise.reject('invalid-username');
        if(!isPassword(password))
            return Promise.reject('invalid-password');

        var entryKey = getEntryKeyFromUsername(username);
        if(storage.general.getItem(entryKey))
            return Promise.reject('user-exists');
        
        return new Promise(function(fulfill, reject){
            var p = promiseScryptHash(password);
            p.done(function(hashed){
                return fulfill(storage.general.setItem(entryKey, {
                    password: hashed,
                }));
            }, function(){
                return reject('storage-error');
            });
        });
    };

    this.changePassword = function(msg){
        var username = msg.username,
            oldPassword = msg.oldPassword,
            newPassword = msg.newPassword;
        if(!isUsername(username))
            return Promise.reject('invalid-username');
        if(!isPassword(oldPassword))
            return Promise.reject('invalid-old-password');
        if(!isPassword(newPassword))
            return Promise.reject('invalid-new-password');

        var entryKey = getEntryKeyFromUsername(username),
            entry = storage.general.getItem(entryKey);
        if(!entry)
            return Promise.reject('invalid-username');

        return new Promise(function(fulfill, reject){
            var p = Promise.all([
                Promise.resolve(entry),
                promiseScryptHash(oldPassword),
                promiseScryptHash(newPassword),
            ]);
            p.done(function(params){
                var entry = params[0];
                var oldPasswordShouldBe = entry.password;
                if(oldPasswordShouldBe != values[1])
                    return reject('invalid-old-password');
                entry.password = params[2];
                return fulfill(storage.general.setItem(entryKey, entry));
            }, function(){
                return reject('storage-error');
            });
            return p;
        });
    };

    this.login = function(msg){
        /* Authenticate a user using given username/password. If success, the
         * user's database will be updated with a UUID for later sessional
         * access.*/
        var username = msg.username, password = msg.password;
        if(!isUsername(username)) 
            return Promise.reject('invalid-username');
        if(!isPassword(password))
            return Promise.reject('invalid-password');

        var entryKey = getEntryKeyFromUsername(username),
            entry = storage.general.getItem(entryKey);
        if(!entry)
            return Promise.reject('authentication-failure');
        
        return new Promise(function(fulfill, reject){
            var p = Promise.all([
                Promise.resolve(entry),
                promiseScryptVerifyHash(password, entry.password),
            ]);
            p.done(function(values){
                var entry = values[0], result = values[1];
                if(true !== result) return reject('authentication-failure');
                var uuid = $crypto$.util.uuid();
                entry.uuid = uuid;
                storage.general.setItem(entryKey, entry).done(function(){
                    fulfill(uuid);
                }, function(){
                    reject('authentication-failure');
                });
            }, function(){
                return reject('storage-error');
            });
        });
    };

    this.logout = function(msg){
        /* Clear the session ID record in user's database record. This
        revokes the access using a given UUID. */
        var username = msg.username, uuid = msg.uuid;
        if(!self.sessionValid(username, uuid)) return;

        var entryKey = getEntryKeyFromUsername(username),
            entry = storage.general.getItem(entry);
        if(!entry) return;

        entry.uuid = false;
        storage.general.setItem(entryKey, entry);
    };

    this.sessionValid = function(username, uuid){
        /* check if a given uuid is still valid. */
        if(!isUsername(username)) return false;
        if(!uuid) return false; // avoid false against false comparation
        var entryKey = getEntryKeyFromUsername(username);
        return storage.general.getItem(entryKey).uuid === uuid;
    };

    // -------- Get private keys
    //          Notice the server will only record private key, which is
    //          assumed to have been encrypted on the client. No public key
    //          will be stored. The publication of public key is done by the
    //          client after its decryption and derivation, through then the
    //          presence mechanism.

    this.setPrivateKey = function(msg){
        var username = msg.username, uuid = msg.uuid;
        var newkey = msg.privateKey;
        if(!self.sessionValid(username, uuid))
            return Promise.reject('need-login');
        if(!isPrivateKey(newkey)) return Promise.reject('invalid-key');
        var entryKey = getEntryKeyFromUsername(username);
        var p = storage.privateKey.setItem(entryKey, newkey);
        return new Promise(function(fulfill, reject){
            p.done(function(){
                fulfill(newkey);
            }, function(){
                reject('storage-error');
            });
        });
    };

    this.getPrivateKey = function(msg){
        var username = msg.username, uuid = msg.uuid;
        if(!self.sessionValid(username, uuid))
            return Promise.reject('need-login');
        var entryKey = getEntryKeyFromUsername(username);
        var entry = storage.privateKey.getItem(entryKey);
        if(!entry) return Promise.reject('no-privatekey');
        return Promise.resolve(entry);
    };


    return this;
};



module.exports = function(storageEngine){
    var storage = storageEngine();
    return new AccountManager(storage);
}
